"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SwordWarrior = void 0;
const ClassHero_1 = __importDefault(require("./ClassHero"));
const ClassHeroSpear_1 = __importDefault(require("./ClassHeroSpear"));
const ClassWeapon_1 = __importDefault(require("./ClassWeapon"));
class HeroSword extends ClassHero_1.default {
    constructor(nameValue, powerValue, lifeValue) {
        super(nameValue, powerValue, lifeValue);
        this._weapon = new ClassWeapon_1.default("Sword", 10);
    }
    attack(opponent) {
        if (opponent instanceof ClassHeroSpear_1.default) {
            this._life -= this._power * 2;
        }
        else {
            super.attack(opponent);
        }
    }
    isKo() {
        return true;
    }
    isAlive() {
        return true;
    }
}
exports.default = HeroSword;
exports.SwordWarrior = new HeroSword("Durion", 200, 50);
//______________________________________________________________________________________________________________________________________
//                                                   | - BRIEF - |
//     `HeroSword` : si le type de `opponent` est `HeroSpear`, multiplier `damage` par deux
