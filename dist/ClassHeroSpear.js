"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpearWarrior = void 0;
const ClassHero_1 = __importDefault(require("./ClassHero"));
const ClassHeroAxe_1 = __importDefault(require("./ClassHeroAxe"));
const ClassWeapon_1 = __importDefault(require("./ClassWeapon"));
class HeroSpear extends ClassHero_1.default {
    constructor(nameValue, powerValue, lifeValue) {
        super(nameValue, powerValue, lifeValue);
        this._weapon = new ClassWeapon_1.default("Spear", 10);
    }
    // attack(opponent: HeroAxe)
    // {
    //    this.attack
    // }
    attack(opponent) {
        if (opponent instanceof ClassHeroAxe_1.default) {
            this._life -= this._power * 2;
        }
        else {
            super.attack(opponent);
        }
    }
    isKo() {
        return true;
    }
    isAlive() {
        return true;
    }
}
exports.default = HeroSpear;
exports.SpearWarrior = new HeroSpear("Jertrude", 200, 50);
//______________________________________________________________________________________________________________________________________
//                                                   | - BRIEF - |
//     `HeroSpear` : si le type de `opponent` est `HeroAxe`, multiplier `damage` par deux
