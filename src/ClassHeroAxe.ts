import Hero from "./ClassHero";
import HeroSword from "./ClassHeroSword";
import Weapon from "./ClassWeapon";



export default class HeroAxe extends Hero
{
    constructor(nameValue: string, powerValue: number, lifeValue: number)
    {
        super(
            nameValue,
            powerValue,
            lifeValue,
        )
        this._weapon = new Weapon("Axe", 10)
    }


    attack(opponent: Hero){
        if(opponent instanceof HeroSword)
        {
            this._life -= this._power *2;
        }
        else
        {
            super.attack(opponent)
        }
    }

    isKo(): boolean {
        return true
    }

    isAlive(): boolean{
        return true
    }
}

export let AxeWarrior = new HeroAxe("Oria", 200, 50,)


//______________________________________________________________________________________________________________________________________
//                                                   | - BRIEF - |

//     `HeroAxe` : si le type de `opponent` est `HeroSword`, multiplier `damage` par deux