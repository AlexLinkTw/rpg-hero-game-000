import Hero from "./ClassHero";
import HeroAxe from "./ClassHeroAxe";
import Weapon from "./ClassWeapon";



export default class HeroSpear extends Hero
{
    constructor(nameValue: string, powerValue: number, lifeValue: number)
    {
        super(
            nameValue,
            powerValue,
            lifeValue,
        )
        this._weapon = new Weapon("Spear", 10)
    }


    // attack(opponent: HeroAxe)
    // {
    //    this.attack
    // }

    attack(opponent: Hero){
        if(opponent instanceof HeroAxe)
        {
            this._life -= this._power *2;
        }
        else
        {
            super.attack(opponent)
        }
    }
    
    isKo(): boolean {
        return true
    }

    isAlive(): boolean{
        return true
    }
}

export let SpearWarrior = new HeroSpear("Jertrude", 200, 50,)


//______________________________________________________________________________________________________________________________________
//                                                   | - BRIEF - |

//     `HeroSpear` : si le type de `opponent` est `HeroAxe`, multiplier `damage` par deux