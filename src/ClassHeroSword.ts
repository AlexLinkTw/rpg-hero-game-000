import Hero from "./ClassHero";
import HeroSpear from "./ClassHeroSpear";
import Weapon from "./ClassWeapon";



export default class HeroSword extends Hero
{
    constructor(nameValue: string, powerValue: number, lifeValue: number)
    {
        super(
            nameValue,
            powerValue,
            lifeValue
        )
        this._weapon = new Weapon("Sword", 10)
    }


    attack(opponent: Hero){
        if(opponent instanceof HeroSpear)
        {
            this._life -= this._power *2;
        }
        else
        {
            super.attack(opponent)
        }
    }
   
    isKo(): boolean {
        return true
    }

    isAlive(): boolean{
        return true
    }
}

export let SwordWarrior = new HeroSword("Durion", 200, 50,)


//______________________________________________________________________________________________________________________________________
//                                                   | - BRIEF - |

//     `HeroSword` : si le type de `opponent` est `HeroSpear`, multiplier `damage` par deux