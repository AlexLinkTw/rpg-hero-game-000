import Weapon from "./ClassWeapon";

console.log("-----Hero game-----");


export default class Hero {

public _name: string
public _power: number
public _life: number
public _weapon?: Weapon


    constructor(nameValue: string, lifeValue: number, powerValue: number)
    {
        this._name = nameValue
        this._power = powerValue + Math.floor(Math.random()*30);
        this._life = lifeValue
        
    }


isKo(): boolean
    {
        return this._life <= 0;
    }

isAlive(): boolean
    {
        return this._life >= 1;
    }
    
attack(opponent: Hero)
    {
        opponent._life -= this._power
    }
}


//______________________________________________________________________________________________________________________________________
//                                                   | - BRIEF - |

// Partie 1 : Héros

// La classe `Hero` permet de créer des objets possédant les propriétés suivantes :

//     name : string
//     power : number
//     life : number
//     attack : function
//     isAlive : function

// La méthode `attack` a un paramètre `opponent` (de type `Hero`). Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.

// Exemple : Si Joan attaque Leon, cela sera représenté par :

// joan.attack(leon)

// La méthode `isAlive` devrait retourner `true` si le nombre de points de vie du héros est supérieur à zéro et `false` sinon.

// Crée deux instances de `Hero` et vérifie que les méthodes `attack` et `isAlive` fonctionnent.